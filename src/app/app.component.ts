import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(public translate: TranslateService) {
    translate.addLangs(['en', 'tl_PH', 'ja_JP']);
    translate.setDefaultLang('en');

    const browserLocale = translate.getBrowserLang();

    if(browserLocale === 'ja_JP') {
      document.dir = 'rtl';
    }

    if(browserLocale.match(/en|tl_PH|ja_JP/)) {
      translate.use(browserLocale);
    }
  }
}
