export default class DashboardPage {
  dashboardTab = '[routerlink="/dashboard"]';
  dashboardHeader = 'app-dashboard > h2';
  searchBox = '#search-box';
  clearBtn = '.clear';
}

export const dashboardPage = new DashboardPage();
