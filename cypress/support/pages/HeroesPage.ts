export default class HeroesPage {
  heroesTab = '[routerlink="/heroes"]';
  heroesHeader = 'app-heroes > h2';
  heroesInputbox = '#new-hero';
  addBtn = '.add-button';
}

export const heroesPage = new HeroesPage();
